import axios from 'axios'

const TRELLO_API_ROOT = process.env.TRELLO_API_ROOT

const axiosTrello = axios.create({
  baseURL: TRELLO_API_ROOT
})

axiosTrello.interceptors.request.use(
  function(config) {
    config.data.key = process.env.TRELLO_KEY
    config.data.token = localStorage.getItem('trelloToken')
    return config
  },
  function(error) {
    return Promise.reject(error)
  }
)

axiosTrello.interceptors.response.use(
  function(response) {
    return response
  },
  function(error) {
    if (401 === error.response.status) {
      console.log('interceptor response 401', error)
      localStorage.removeItem('trelloToken')
      document.location.reload()
    } else {
      return Promise.reject(error)
    }
  }
)

const createBoard = name =>
  axiosTrello.post('/boards', {
    name,
    defaultLists: false
  })

const addList = (board, name, pos) =>
  axiosTrello.post('/lists', {
    idBoard: board,
    name,
    pos
  })

const addCard = (list, name, pos) =>
  axiosTrello.post('/cards', {
    idList: list,
    name,
    pos
  })

const addImage = (card, url) =>
  axiosTrello.post(`/cards/${card}/attachments`, {
    url
  })

export default {
  createBoard,
  addList,
  addCard,
  addImage
}
