const path = require('path')
const pkg = require('./package')
const Dotenv = require('dotenv-webpack')

module.exports = {
  entry: ['src/polyfills.js', 'src/index.js'],
  html: {
    title: pkg.productName,
    description: pkg.description,
    template: path.join(__dirname, 'index.ejs')
  },
  postcss: {
    plugins: [require('tailwindcss')('./tailwind.js'), require('autoprefixer')]
  },
  presets: [
    require('poi-preset-bundle-report')(),
    require('poi-preset-offline')({
      pwa: './src/pwa.js', // Path to pwa runtime entry
      pluginOptions: {} // Additional options for offline-plugin
    })
  ],
  webpack(config) {
    config.plugins.push(
      new Dotenv({
        systemvars: true
      })
    )
    return config
  }
}
